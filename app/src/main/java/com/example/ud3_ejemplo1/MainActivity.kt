package com.example.ud3_ejemplo1

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    // Método que lanza un intent implícito a la web de Google.
    fun abrirIntent(view: View) {
        // Creamos el intent y le asignamos la acción y los datos (la URI a abrir).
        val intent = Intent().apply {
            action = Intent.ACTION_VIEW
            data = Uri.parse("https://www.google.com")
        }

        // Verificamos si existe una aplicación para gestionarlo.
        if(intent.resolveActivity(packageManager) != null){
            startActivity(intent)
        }
    }
}